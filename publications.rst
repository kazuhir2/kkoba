
研究業績
==============================================


論文（査読付き）
------------------------------------------------------------------------------------- 
- T.Oyama, Nicholas G. Hall, K.Kobayashi, "Generalized Parametric Divisor Method for Political Apportionment", International Transactions in Operational Research, `Article　<https://onlinelibrary.wiley.com/doi/abs/10.1111/itor.12622?af=R>`_ 2018. 
- M.Tanaka and K.Kobayashi, "A Route Generation Algorithm for an Optimal Fuel Routing Problem between Two Single Ports", International Transactions in Operational Research, 26, 529-550, 2019 
- 小林 和博, 成澤 龍人, 安井 雄一郎, 藤澤 克樹, “辞書式最速流による避難計画作成モデルの実験的解析”, 日本オペレーションズ・リサーチ学会和文論文誌，59, 86-105, 2016
- K.Kobayashi and M.Kubo,“Optimization of Oil Tanker Schedules by Decomposition, Column Generation, and Time-Space Network Techniques”, Japan Journal of Industrial and Applied Mathematics, 27, 161-173, 2010
- K.Kobayashi, H.Morohosi and T. Oyama, “Applying Path-Counting Methods for Measuring the Robustness of the Network-Structured System”, International Transactions in Operational Research, 16, 371-389, 2009
- K.Kobayashi, S.Kim and M.Kojima, “Sparse Second Order Cone Programming Formulations for Convex Optimization Problems”,Journal of the Operations Research Society of Japan, 51, 241-264, 2008
- K.Kobayashi, S.Kim and M.Kojima, “Correlative Sparsity in Primal-Dual Interior-Point Methods for LP, SDP and SOCP”, Applied Mathematics & Optimization, 58, 69-88, 2008
- K.Kobayashi, K.Nakata and M.Kojima, “A Conversion of an SDP Having Free Variables into the Standard Form SDP”, Computational Optimization and Applications, 36, 289-307, 2007

会議論文（査読付き)
-------------------------------------------------------------------------------------
- M.Tanaka and K.Kobayashi, “Perspective Reformulation for Optimal Fuel Routing Problem”, Proceedings of the International Symposium on Scheduling 2015, 89-94, 神戸, 2015年7月
- K.Kobayashi, “A Linear Approximation of the Value Function of an Approximate Dynamic Programming Approach for the Ship Scheduling Problem”,Learning and Intelligent OptimizatioN (LION4), Lecture Notes in Computer Science (LNCS) 6073, 184-187, C. Blum and R. Battiti (eds.), Springer, ベニス，イタリア，2010年1月
- T.Seta, K.Kobayashi, and M.Kubo, “Ship Scheduling in the Steel Industry – a Rolling Horizon and Approximate Dynamic Programming Approach -“, Proceedings of the International Symposium on Scheduling 2009, 18-21, 名古屋，2009年7月
- K.Kobayashi, T.Kano and M.Kubo,”A Two-Phase Algorithm for Tramp Ship Routing Problems by a Column Generation Approach”, Learning and Intelligent OptimizatioN (LION3), Online Proceedings, トレント，イタリア，2009年1月
- K.Kobayashi, “Computational Results on Some Shortest Path Problems with Side Constraints”, Proceedings of SCIE Annual Conference 2008, 1B17-4, 東京，2008年8月

論文（査読無し）
-------------------------------------------------------------------------------------
- M.Tanaka and K.Kobayashi, “MISOCP Formulation and route generation algorithm for ship navigation problem”, Technical Report No.2013-8, Department of Industrial Engineering and Management, Tokyo Institute of Technology, 2013
- M.Yamashita, K.Fujisawa, K.Nakata, M.Fukuda, K.Kobayashi and K.Goto, “A High-Performance Software Package for Semidefinite Programs : SDPA 7”, Research Report on Mathematical and Computing Sciences B-460，Department of Mathematical and Computing Sciences, Tokyo Institute of Technology, 2010
- K.Fujisawa, M.Fukuda, K.Kobayashi, M.Kojima, K.Nakata and M.Nakata, “SDPA(SemiDefinite Programming Algorithm) User’s Manual – Version 7.0.5”, Research Report on Mathematical and Computing Sciences B-448, Department of Mathematical and Computing Sciences, Tokyo Institute of Technology, 2008

著書，訳書
-------------------------------------------------------------------------------------
- 小林和博，“配送計画”, 第3章 ネットワークの世界”，『空間解析入門』，貞広幸雄，山田育穂，石井儀光（編），朝倉書店，2018年．
- 久保幹雄，小林和博，斉藤努，並木誠，橋本英樹，『Python言語によるビジネスアナリティクス-実務家のための最適化・統計解析・機械学習-』，近代科学社，2016年.
- 小林和博, “第6章 人道支援サプライチェーンにおける数理モデルとその既存研究”，『サプライチェーンリスク管理と人道支援ロジスティクス』久保幹雄，松川弘明（編），近代科学社，2015年．
- 和田忠，小林和博，『航海応用力学の基礎（3訂版）』，成山堂書店，2015年3月
- M.Yamashita, K.Fujisawa, M.Fukuda, K.Kobayashi, K.Nakata and M.Nakata, “Latest Developments in the SDPA Family for Solving Large-Scale SDPs”,『Handbook on Semidefinite, Cone and Polynomial Optimization : Theory, Algorithms, Software and Applications』, Miguel F. Anjos and Jean B. Lasserre (eds.), Springer, 2011
- ジョン・V・グッターグ，『Python言語によるプログラミングイントロダクション：世界標準MIT教科書』，麻生敏正，木村泰紀，小林和博，関口良行，並木誠，藤原洋志（翻訳），久保幹雄（監訳），近代科学社，2014年12月

研究報告等
-------------------------------------------------------------------------------------
- 小林和博，“輸送システムに関する最適化技術”，海上技術安全研究所報告，14(4)，45-62，2015
- 間島隆博，小坂浩之，小林和博，“国際海上コンテナ輸送のネットワーク分析と流動モデルの開発”，海上技術安全研究所報告，14(4)，17-26，2015
- 小林和博，“二目的最短路問題の非劣解の生成法”，海上技術安全研究所報告，12(3)，67-73，2012
- 久保幹雄，小林和博，“物流と数理計画”，海上技術安全研究所報告，7(4)，83-86，2008
- 小林和博，“配船アルゴリズム”，海上技術安全研究所報告，7(4)，73-76，2008

解説等
-------------------------------------------------------------------------------------
-  久保幹雄，小林和博，武田朗子，田中未来，村松正和，“サプライ・チェイン最適化における2次錐最適化の応用”，オペレーションズ・リサーチ，59（12），739-747, 2014
- 小林和博，“船舶スケジューリング数理モデル作成の具体的手順”，57(4)，205-210, 2012
- 小林和博，“船舶スケジューリング効率化の技術について”，船と海のサイエンス，2011冬号，10-12，2011
- 久保幹雄，小林和博, “階層的積木法と列生成法の融合-輸送・船舶スケジューリングを例として-“，計測自動制御学会学会誌「計測と制御」，47，519-524, 2008
- 中田和秀，藤澤克樹，福田光浩，山下真，中田真秀，小林和博，“最適化ソフトウェアSDPA”，日本応用数理学会学会誌「応用数理」，18，2-14，2008

ソフトウェア開発
-------------------------------------------------------------------------------------
- 半正定値最適化ソフトウェア SDPA 開発メンバー
  SDPAは，主双対内点法で半正定値最適化問題を解くソフトウェアである。その開発メンバーとしてソフトウェアの機能拡張及び性能向上に取り組んでいる。

外部研究資金
-------------------------------------------------------------------------------------
- 科学研究費補助金基盤研究(C)，過不足のない効率的な海上輸送実現のための最適化・シミュレーションシステムの構築，平成29年度-平成31年度, 研究代表者
- 科学研究費補助金基盤研究(B)，エネルギー資源の輸入計画のための統合的リスク評価モデルの開発（研究代表者：中央大学 鳥海重喜准教授），平成29-32年度，研究分担者
- 科学研究費補助金基盤研究(B),  スマートシティ実現のための多階層型データ解析及び最適化システムの開発と評価（研究代表者：九州大学 藤澤克樹教授），平成28-32年度，連携研究者（分担金なし）
- 科学研究費補助金基盤研究(A),  スマートシティ実現のための多階層型データ解析及び最適化システムの開発と評価（研究代表者：九州大学 藤澤克樹教授），平成28-32年度，連携研究者（分担金なし）
- 科学研究費補助金基盤研究(B)，マルチエージェントシステムによる帰宅・通勤困難者のための路線網構築構築法（研究代表者：海上技術安全研究所 間島隆博主任研究員），平成25-27年度，研究分担者
- 科学研究費補助金基盤研究(C)，リスク・マネジメントに対応したサプライ・チェイン最適化モデルの開発（研究代表者：東京海洋大学 久保幹雄教授），平成23-25年度，研究分担者
- 科学研究費補助金若手研究(B)，多品種フロー問題によるコンテナ船航路ネットワーク設計手法の開発，平成21-22年度，研究代表者
- 科学研究費補助金基盤研究(C)，在庫と不確実性を考慮した船舶スケジューリングモデル構築（研究代表者：東京海洋大学 久保幹雄教授），平成20-22年度，研究分担者

受賞
-------------------------------------------------------------------------------------
- 2010年　スケジューリング学会技術賞

学位論文
-------------------------------------------------------------------------------------
- K.Kobayashi, Sparsity Exploitation in Primal-Dual Interior-Point Methods for Conic Linear Optimization Problems, 博士（理学），東京工業大学大学院情報理工学研究科，2009年3月

招待講演/依頼公演
-------------------------------------------------------------------------------------
- 小林和博，“ネットワークと動的最適化"，2017年度第1回ORセミナー『Python言語によるビジネスアナリティクス』，東京，2017年5月
- 小林和博，“ネットワークと動的最適化"，2016年度第3回ORセミナー，『Python言語によるビジネスアナリティクス』，東京，2016年9月
- 小林和博，“辞書式最速流による避難計画作成モデルの実験的解析”，日本オペレーションズ・リサーチ学会研究部会「公共的社会システムとOR」第8回研究会，東京，2015年6月
- 小林和博，“普遍的最速流アルゴリズム実装の詳細と課題”，九州大学マス・フォア・インダストリ研究所共同利用研究（短期共同研究）「大規模データに対する最大フロー求解アルゴリズムの実装技術の構築」に関する公開ワークショップ，福岡，2015年6月
- 小林和博, “Pythonを用いた最適化”，『Python言語によるプログラミング・イントロダクション』出版記念 Python活用セミナー，東京，2015年5月
- 小林和博，“人道支援サプライ・チェインにおける数理モデルとその既存研究”，サプライ・チェイン・リスク管理と人道支援ロジスティクスセミナー第3回，東京，2014年5月
- 小林和博，“移動時間と燃料消費量の不確実性に対応するためのロバスト最短路モデル”，サプライ・チェイン・リスク管理と人道支援ロジスティクスセミナー第2回，東京，2013年7月
- 小林和博，“船舶スケジューリング数理モデル作成の具体的手順”，新宿OR研究会，東京，2012年7月
- 小林和博，“近似動的計画法入門”，チュートリアル講演，スケジューリング・シンポジウム2010，東京，2010年9月
- 小林和博，“船舶スケジューリングと船の最短路問題”，日本オペレーションズ・リサーチ学会研究部会「計算と最適化の新展開」2009年度第5回研究会，東京，2010年2月
- 小林和博，久保幹雄，“船舶スケジューリング”，第20回RAMPシンポジウム，東京，2008年10月
- 小林和博，“海上ロジスティクス工学-数理最適化技術による輸送の効率化-”，神戸大学海事科学研究科国際海上輸送システム創出の研究シンポジウム，神戸，2008年2月

共同研究
-------------------------------------------------------------------------------------

**大学**

- 平成29年度 九州大学マス・フォア・インダストリ研究所共同利用研究計画

　 プロジェクト研究「よりよい都市・社会構築のための基盤技術としての離散最適化の研究」 `[詳細] <https://www.imi.kyushu-u.ac.jp/joint_researches>`_

  プロジェクト代表者：小林和博，神山直之（九州大学）

- 平成28年度 九州大学マス・フォア・インダストリ研究所 短期共同研究  `[詳細] <http://www.imi.kyushu-u.ac.jp/joint_research/detail/20160019>`_

      研究計画題目：最大フロ－求解アルゴリズムの効率的実装と，その大規模データを用いた避難計画策定への応用
      研究代表者：小林和博（東京理科大学理工学部経営工学科・講師）
      研究実施期間：平成28年9月21日（水）～ 平成28年9月23日（金）

- 東京海洋大学，九州大学，中央大学

**企業など**

- 新日本製鐡（株），日鐵物流（株），旭タンカー（株），西部タンカー（株），宇部三菱セメント（株），出光興産（株），日本気象協会，鉄道総合技術研究所

口頭発表
-------------------------------------------------------------------------------------
- 田中未来，小林和博，“燃料最小化航路計画問題のMISOCP定式化とルート生成アルゴリズム，日本応用数理学会2016年度年会，北九州国際会議場，2016年9月
- 田中未来，小林和博，“燃料最小化航路計画問題に対するルート生成アルゴリズム"，日本オペレーションズ・リサーチ学会2016年秋季研究発表会，山形大学，2016年9月
- Mirai TANAKA, Kazuhiro KOBAYASHI, MISOCP Formulation for the Optimal Fuel Routing Problem and the Route Generation Algorithm, ICCOPT 2016, Tokyo, August, 2016
- 小林和博，“非線形な目的関数をもつ船舶スケジューリングに対するルート列挙解法”，日本オペレーションズ・リサーチ学会2015年春季研究発表会，東京，2015年3月
- 小林和博，成澤龍人，安井雄一郎，藤澤克樹，“緊急避難計画に対する普遍的最速流の実験的解析”，スケジューリング・シンポジウム2014，富山，2014年9月
- 田中未来，久保幹雄，小林和博，武田朗子，村松正和，“不確実性を考慮したサプライ・チェイン最適化モデル”，スケジューリング・シンポジウム2014，富山，2014年9月
- 小林和博，“MISOCPを部分問題とする船舶スケジューリング”，日本オペレーションズ・リサーチ学会2014年春季研究発表会，大阪，2014年3月
- M.Tanaka and K.Kobayashi, “MISOCP Formulation and Route Generation Algorithm for Ship Navigation Problem”, The 9~th International Conference on Optimization : Techniques and Application, SA-1, 台湾，2013年12月
- 成澤龍人，安井雄一郎，藤澤克樹，小林和博，“最速フローを用いた避難所の評価”，日本オペレーションズ・リサーチ学会2013年秋季研究発表会，徳島，2013年9月
- 田中未来，小林和博，“船舶航行計画問題に対する航路生成法”，日本オペレーションズ・リサーチ学会2013年秋季研究発表会，徳島，2013年9月
- 小林和博，“動的多品種流によるコンテナ輸送フローの算出”，日本オペレーションズ・リサーチ学会2013年秋季研究発表会，徳島，2013年9月
-  小林和博，“運航時間制約付き航路計画問題に対するPerspective再定式化”，日本オペレーションズ・リサーチ学会2013年春季研究発表会，東京，2013年3月
- K.Kobayashi, “Alternative Objective functions in ship scheduling for managing supply chain disruption risk”, International Symposium on Mathematical Programming(ISMP) 2012，ドイツ，2012年8月
- K.Kobayashi, T.Seta and M.Kubo, “An Approximate Dynamic Programming Approach for Ship Scheduling Problem”, 20~th International Symposium on Mathematical Programming (ISMP) 2009, アメリカ合衆国，2009年8月
- K.Kobayashi, M.Kubo, T.Kano, “A Two-Phase Approach for a Tramp Ship Scheduling Problem”, SIAM Conference on Optimization 08, アメリカ合衆国，2008年5月
-  軽海秀晃，久保幹雄，小林和博，山口氏嗣，鄭金花，“鉄鋼業界における船舶スケジューリング”，スケジューリング・シンポジウム2008，東京，2008年9月
-  小林和博，久保幹雄，加納敏幸，“内航ケミカルタンカー船隊に対する配船計画問題”，平成19年日本船舶海洋工学会秋季講演会西部支部，愛媛，2007年11月
- K.Kobayashi, “Correlative Sparsity in Primal-Dual Interior-Point Methods and Efficient SOCP formulations”， The Second International Conference on Continuous Optimization of the Mathematical Programming Society (ICCOPT 07/MOPTA 07), カナダ，2007年8月
 　　　　
- K.Kobayashi, S.Kim and M.Kojima, “Correlative Sparsity in Primal-Dual Interior-Point Methods and Efficient SOCP Formulations”，Workshop on Advances in Optimization, 東京，2007年4月

参加研究プロジェクト
-------------------------------------------------------------------------------------
- 国土交通省 建設技術研究開発助成制度 「建設現場におけるスマートウェアを用いた安心・安全及び生産性向上IoTシステムの開発」研究分担者
- 新エネルギー・産業技術総合開発機構（NEDO）先導研究「内航船の環境調和型運航計画支援システムに関する研究開発」
- 環境省　CO2排出削減対策強化誘導型技術開発・実証事業「航海・配船計画支援システム導入による船舶からのCO2 排出削減実証事業」

スキル
-------------------------------------------------------------------------------------
- 言語 日本語（母国語），英語
- 計算機ソフトウェア：C, C++, Python, TeX, MATLAB，Office
- 計算機OS：Mac OS X, Linux, Windows, AIX, OS/390

所属学会
-------------------------------------------------------------------------------------
- Society of Industrial and Applied Mathematics(SIAM), 日本応用数理学会，日本オペレーションズ・リサーチ学会，

学会での委員活動
-------------------------------------------------------------------------------------
- 2017年9月- スケジューリング学会表彰委員長
- 2017年4月- 日本応用数理学会編集委員
- 2010年5月-2017年3月 日本オペレーションズ・リサーチ学会庶務幹事
- 2007年4月-2011年3月 情報処理学会ハイパフォーマンスコンピューティング研究会運営委員．
- International Symposium on Scheduling 2017, International Program Committee
- International Symposium on Scheduling 2015, International Program Committee
