.. Kazuhiro KOBAYASHI documentation master file, created by
   sphinx-quickstart on Tue Feb  9 00:46:29 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kazuhiro KOBAYASHI 小林 和博
==============================================

.. .. image:: kk-mono.jpg
..  :width: 20%

`東京理科大学 <http://www.tus.ac.jp>`_ 理工学部経営工学科講師

Junior Associate Professor, `Tokyo University of Science <http://www.tus.ac.jp>`_

`CV <https://tus.box.com/s/jtvimu3ikrk220ycz66a6voxxrxr092z>`_


.. toctree::
   :maxdepth: 1


   cv
   publications
   researchnote
   diary
   techmemo
   lecture

.. `Den String Orchestra 2016 <https://sites.google.com/site/denstringorchestra/dso2016>`_

.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
