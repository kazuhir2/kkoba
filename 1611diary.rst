2016年11月
===========================

.. _day161130

2016年11月30日（水） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


.. _day161129

2016年11月29日（火） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 門司から明日の講演のために上京してきた古い友人と会った．もはや何屋かわからない彼は愉快だ．
- ３年生のセミナーでosmnxをインストールしてもらおうとしたが，うまくいかない．osmnxはインストールに失敗する例が多い．

.. _day161128

2016年11月28日（月）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 必修の科目が3コマある月曜日．
- 200人近くの必修科目になると，やる気のある学生が精一杯勉強できる環境をいかに維持するかが重要になってくる．切り札的な方法は難しかろう．

.. _day161127

2016年11月27日（日）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- アマデウスの練習に参加．今回は，ブラームスのピアノ協奏曲２曲．特に木管の皆様がうまくて驚いた．楽しかった．

.. _day161125

2016年11月25日（金）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



.. _day161124

2016年11月24日（木）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 降雪で駅に着くのが遅れ，２本くらい後の電車に乗った．
- OR講義１０回目.多品目ロットサイズ決定問題を扱った．


.. _day161123

2016年11月23日（水）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 人生の休日．楽譜見たり，ブロックしたり，算数の問題解いたり．
- 原稿やったり．


.. _day161122

2016年11月22日（火）雨のち晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _day161121

2016年11月21日（月）小雨
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 土日に学園祭があったということで，今日は休み
- 学科の先生方と勉強会．電力関係の市場の均衡の話．

.. _day161118

2016年11月18日（金）晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- IBIS2016三日目
- 甘利先生の招待講演など．堪能した．全く，勉強にしかならない．
- 終了直後の新幹線で東京に戻ったあと，少し仕事．
- 終電間際に，キャピキャピしたレストランでおっさん一人で食事．財布と味を考えると，ここが良い．雰囲気を壊してしまってすみません．
- IBISで，勉強進めたい欲が増した．大学からの発表者は，圧倒的に国公立大所属の方々が多かった．

.. _day161117

2016年11月17日（木）晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- IBIS2016二日目

- 出張書類作成に手間取った
- ２限　３年生セミナー．制約付き最短路ソルバを，VBAのゴールシークでサクッと作る履修生はたのもしい

.. _day161114

2016年11月14日（月）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 2限 経営工学概論2（１年生必修）
- 4限 卒研進捗報告（4年生必修）
- 5限 線形代数演習B（1年生必修） - 固有値と固有ベクトル!!

こう書いてみると，なかなかすさまじいですね．


2016年11月13日（日） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 人生の休日



.. _day161112

2016年11月12日（土） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


- 2016年度第3回ORセミナー(http://www.orsj.or.jp/activity/seminar.html)で講演をしに中野の構造計画研究所に出かける


  2016年度　第3回
  『Python言語によるビジネスアナリティクス』

  開催趣旨
  高級スクリプト言語"Python"を用いて，実務家のための最適化・統計分析・機械学習などのオペレーションズ・リサーチの分野で必要なツールをご紹介します．このセミナーは，これからPythonを始める方や，これまで別のソフトウェアで最適化やデータ分析をしていた方にとって，Pythonの世界に踏み出すための最適なイントロダクションとなるでしょう．Pythonのユーザーフレンドリーさと自由度の高さを体験しましょう．


.. _day161111

2016年11月11日（金） 雨のち曇り
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 査読２件
- プログラミングを粛々と進める．
- 明日のORセミナースライドのチェック

.. _day161110

2016年11月10日（木） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 午前は，書類作成，プログラミング
- 午後は，海洋大で勉強会参加
- QGISというGISソフトを知った．


.. _day161109

2016年11月9日（水） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 午前は，書類作成，プログラミング
- 午後は，六本木に研究打ち合わせのために出かける
- 蕎麦屋で香水大盛りのおじさまが隣にやってきてしょんぼり．大好物のカレー南蛮も，香水の味しかしなかった．たまの楽しみだったのに，，，

.. _day161108


2016年11月8日（火） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- ３年生のセミナ．なかなか逞しくて，私も見ていて機嫌がよくなる．
- T先生と研究の話．楽しかった．楽しかっただけでなく，研究の具体的ネタにしなければならない．

.. _day161107

2016年11月7日（月） 晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 今年度で退職の教授の連続講義で，卒論から修論くらいの研究内容を聞き，クオリティの高さにくらくらする．

- 卒研進捗報告会．卒研を自分のこととして取り組んでいる学生と，ほとんど他人事の学生がいる．どう取り組むかは自分の問題なので自分で決めればよいが，その帰結についてはよく考える方がよい．

- 自分が学生の時に大講堂で受けていた統計の講義は数百人くらい学生がいたが，どんな状態だっただろう?．あまり思い出せない．板書と講義（トーク）だったと記憶しているが，学生サイドは静かに聞いたいただろうか？　当時は，聞く気のない学生はそもそも部屋に来ていない，という可能性が高いか．

- 某とても楽しそうなオケへの出演をお誘いいただいたが，大学の入学式と重なるので泣く泣く断念．前回も講義日と重なって参加できなかった．祝日の講義のために遊びを断念することが多い．

.. _day161106

2016年11月6日（日）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 絵を描いたりブロックで昆虫を作ったり作曲したり．


.. _day161105

2016年11月5日（土）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 神楽坂キャンバス近くの森戸記念館で，WOO研究会に参加．会場は，夕方になると近隣の飲食店からの調理の匂いが漂ってきてお腹が減る．

- 神楽坂近くは着物を着た若い男女がいつもより多かった．何かのイベントかしら．


.. _day161104

2016年11月4日（金）
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 講義資料の作成など．

- OSMnxの使い方をいろいろやっている．実験結果の描画に便利．２点間の道路上の距離の測定にも便利．


.. _day161103

2016年11月3日(木)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 今日は国民の休日であるけれど，講義日なので，講義をしに行く．3年生対象のオペレーションズ・リサーチ1をやった．今日は，安全在庫配置モデルをやった．のーとをとって勉強する人と，そうでない人と．定式化の説明をしていたら，教壇までやってきて，「すいません」と話しかけられたから，どんな緊急事態かと思って話を中断したら，「出席カードください」．

- 今日は祝日だから通勤電車はすいているだろうと思ったらそうでもなかった．旅行者と，スーツ姿の（おそらく）通勤客で，普段と変わらなかった．

- 年末調整書類提出だん．

- 秋葉原駅駅前に，Bose quiet comfort 35の体験ブースがあった．ストレスが激減するので，おすすめ．


.. _day161101

2016年11月1日(火) 雨のち晴れ
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- 研究室入り口横の研究紹介ポスターを更新．外注して綺麗なやつを貼るつもりだが，そろそろ来年度の卒研配属を考え始める時期なので，仮に大判プリンタで印刷したものを貼っておくことにした．

- 某研究助成の申請書作成だん．

- Bose quietcomfort 35はマイク内蔵なので，skypeなどの通話では単体で使うことができる。
