
日録
==============================================

2018年
------------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 1

   1804diary.rst
   1801diary.rst

2017年
------------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 1

   1712diary.rst
   1707diary.rst
   1706diary.rst
   1705diary.rst
   1704diary.rst
   1703diary.rst
   1702diary.rst
   1701diary.rst

2016年
------------------------------------------------------------------------------------

.. toctree::
   :maxdepth: 1

   1612diary.rst
   1611diary.rst
   1610diary.rst
   1607diary.rst
   1606diary.rst
   1605diary.rst
   1604diary.rst
   1603diary.rst
   1602diary.rst
   1601diary.rst

2015年
-------------------------------------------------------------------------------------
- `2015年12月 <http://mathopt.sakura.ne.jp/wp/%E6%97%A5%E9%8C%B2/%E9%81%8E%E5%8E%BB%E3%81%AE%E6%97%A5%E9%8C%B2/2015-2/2015-12/>`_
- `2015年11月 <http://mathopt.sakura.ne.jp/wp/%E6%97%A5%E9%8C%B2/%E9%81%8E%E5%8E%BB%E3%81%AE%E6%97%A5%E9%8C%B2/2015-2/2015-11/>`_
- `2015年10月以前 <http://mathopt.sakura.ne.jp/wp/%E6%97%A5%E9%8C%B2/%E9%81%8E%E5%8E%BB%E3%81%AE%E6%97%A5%E9%8C%B2/>`_
